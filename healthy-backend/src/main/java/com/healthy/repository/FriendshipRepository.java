package com.healthy.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.healthy.entity.Friendship;

@Repository
public interface FriendshipRepository extends JpaRepository<Friendship, Long> {
	@Query(value = "SELECT a.email,f.status, up.full_name, up.avatar_url ,f.send_mail, f.receive_mail FROM account a "
			+ "LEFT JOIN (SELECT CASE " + "WHEN f.send_mail = :email THEN f.receive_mail "
			+ "WHEN f.receive_mail = :email THEN f.send_mail ELSE :email END AS email, f.status, :email AS original_email ,f.send_mail, f.receive_mail "
			+ "FROM friendship f WHERE (f.send_mail = :email AND f.receive_mail LIKE CONCAT('%',:search,'%')) "
			+ "OR (f.receive_mail = :email AND f.send_mail LIKE CONCAT('%',:search,'%'))) AS f ON f.email = a.email "
			+ " LEFT JOIN user_profile up ON a.account_id = up.account_id "
			+ "WHERE a.email LIKE CONCAT('%',:search,'%') AND !(a.email = :email)", nativeQuery = true)
	List<Map<String, String>> findFriendshipStatus(@Param("email") String email, @Param("search") String search);

	@Query(value = "SELECT a.email,f.status, up.full_name, up.avatar_url ,f.send_mail, f.receive_mail FROM account a "
			+ "LEFT JOIN (SELECT CASE " + "WHEN f.send_mail = :email THEN f.receive_mail "
			+ "WHEN f.receive_mail = :email THEN f.send_mail ELSE :email END AS email, f.status, :email AS original_email ,f.send_mail, f.receive_mail "
			+ "FROM friendship f WHERE ((f.send_mail = :email AND f.receive_mail LIKE CONCAT('%',:search,'%')) "
			+ "OR (f.receive_mail = :email AND f.send_mail LIKE CONCAT('%',:search,'%')) AND f.status = 0)) AS f ON f.email = a.email "
			+ " LEFT JOIN user_profile up ON a.account_id = up.account_id "
			+ "WHERE a.email LIKE CONCAT('%',:search,'%') AND !(a.email = :email)", nativeQuery = true)
	List<Map<String, String>> findFriendshipRequest(@Param("email") String email, @Param("search") String search);

	@Query(value = "SELECT a.email, up.full_name, up.avatar_url, g.weight_start, g.weight_finish ,up.weight, d.stt_remaining, f.status FROM account a "
			+ "INNER JOIN (SELECT CASE " + "WHEN f.send_mail = :email THEN f.receive_mail "
			+ "WHEN f.receive_mail = :email THEN f.send_mail ELSE :email END AS email, f.status, :email AS original_email "
			+ "FROM friendship f WHERE ((f.send_mail = :email AND f.receive_mail LIKE CONCAT('%',:search,'%')) "
			+ " OR (f.receive_mail = :email AND f.send_mail LIKE CONCAT('%',:search,'%'))) AND f.status = 1) AS f ON f.email = a.email "
			+ "LEFT JOIN (SELECT * FROM day WHERE  date = :date ) d ON d.account_id = a.account_id "
			+ " LEFT JOIN user_profile up ON a.account_id = up.account_id " + "LEFT JOIN goal g ON g.email = a.email "
			+ "WHERE !(a.email = :email) AND a.email LIKE CONCAT('%',:search,'%')", nativeQuery = true)
	List<Map<String, String>> searchAllFriend(@Param("email") String email, @Param("date") Long date,
			@Param("search") String search);

	@Query(value = "SELECT a.email, up.full_name, up.avatar_url, g.weight_start, g.weight_finish ,up.weight, d.stt_remaining, f.status FROM account a "
			+ "INNER JOIN (SELECT CASE " + "WHEN f.send_mail = :email THEN f.receive_mail "
			+ "WHEN f.receive_mail = :email THEN f.send_mail ELSE :email END AS email, f.status, :email AS original_email "
			+ "FROM friendship f WHERE ((f.send_mail = :email AND f.receive_mail LIKE CONCAT('%',:search,'%')) "
			+ " OR (f.receive_mail = :email AND f.send_mail LIKE CONCAT('%',:search,'%'))) AND f.status = 1) AS f ON f.email = a.email "
			+ " LEFT JOIN user_profile up ON a.account_id = up.account_id "
			+ "LEFT JOIN  (SELECT * FROM day WHERE  date = :date ) d ON d.account_id = a.account_id "
			+ "LEFT JOIN goal g ON g.email = a.email "
			+ "WHERE d.stt_remaining = :sttRemaining AND !(a.email = :email) "
			+ "AND  a.email LIKE CONCAT('%',:search,'%')", nativeQuery = true)
	List<Map<String, String>> searchFriendBySttRemaining(@Param("email") String email,
			@Param("sttRemaining") Integer sttRemaining, @Param("search") String search, @Param("date") Long date);

	@Query(value = "FROM Friendship f WHERE (f.sendMail = :sendMail AND f.receiveMail = :receiveMail) "
			+ "OR (f.sendMail = :receiveMail AND f.receiveMail = :sendMail)")
	Friendship findByBothEmail(@Param("sendMail") String fromEmail, @Param("receiveMail") String toEmail);

	@Query(value = "SELECT receive_mail FROM healthy.friendship WHERE send_mail = :email AND status = 1" + "       UNION"
			+ "       SELECT send_mail FROM healthy.friendship WHERE receive_mail = :email AND status = 1", nativeQuery = true)
	List<String> findListEmailFriend(@Param("email") String email);

}
