package com.healthy.api;

import lombok.Getter;
import lombok.Setter;

public class RemainApi {
    @Getter
    @Setter
    public static class Request {
        private Long date;
    };

    @Getter
    @Setter
    public static class Response {
        private Integer status;
        private String title;
    };
}
