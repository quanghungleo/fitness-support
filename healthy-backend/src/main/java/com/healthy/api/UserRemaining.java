package com.healthy.api;

import com.healthy.entity.Account;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class UserRemaining {
    @Getter
    @Setter
    public static class Request {
        private Long date;
        private Integer status;
    }

    @Getter
    @Setter
    public static class Response {
        private List<Account> lists;
    }
}

