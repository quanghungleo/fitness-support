package com.healthy.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.healthy.entity.Friendship;
import com.healthy.model.FriendshipStatus;
import com.healthy.model.NotifyDto;
import com.healthy.model.ResponseObject;
import com.healthy.repository.AccountRepository;
import com.healthy.service.FriendshipService;
import com.healthy.service.NotifyService;

@RestController
@RequestMapping("/api/friendship")
public class FriendshipController {
	@Autowired
	private FriendshipService friendshipService;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private NotifyService notifyService;

	@GetMapping(value = "/getAll")
	public ResponseObject<List<FriendshipStatus>> getAll(@RequestParam("search") String search,
			HttpServletRequest request) {
		String rsEmail = request.getAttribute("rsEmail").toString();
		return new ResponseObject<>(friendshipService.findAll(rsEmail, search), true, "Get list friend successfully.");
	}

	@GetMapping(value = "/getAllFriend")
	public ResponseObject<List<FriendshipStatus>> getAll(@RequestParam("search") String search,
			@RequestParam("date") Long date, HttpServletRequest request) {
		String rsEmail = request.getAttribute("rsEmail").toString();
		return new ResponseObject<>(friendshipService.findAllFriend(rsEmail, search, date), true,
				"Get list friend successfully.");
	}

	@GetMapping(value = "/getAllByStatus")
	public ResponseObject<List<FriendshipStatus>> getAllByStatus(@RequestParam("search") String search,
			@RequestParam("date") Long date, @RequestParam("sttRemaining") Integer sttRemaining,
			HttpServletRequest request) {
		String rsEmail = request.getAttribute("rsEmail").toString();
		return new ResponseObject<>(friendshipService.findAllFriendByStatus(rsEmail, search, date, sttRemaining), true,
				"Get list friend successfully.");
	}

	@PostMapping(value = "/create")
	public ResponseObject<List<FriendshipStatus>> create(@RequestBody FriendshipStatus friendshipStatus,
			HttpServletRequest request) {
		String rsEmail = request.getAttribute("rsEmail").toString();
		Friendship friendship = Friendship.builder().sendMail(rsEmail).receiveMail(friendshipStatus.getReceiveEmail())
				.status(false).build();
		friendshipService.save(friendship);
		NotifyDto notifyDto = NotifyDto.builder().email(friendshipStatus.getReceiveEmail()).title("Lời mời kết bạn")
				.message(rsEmail + " muốn kết bạn với bạn.").build();
		notifyService.sendNotify(notifyDto);
		return new ResponseObject<>(null, true, "Create friendship successfully.");
	}

	@PutMapping(value = "/accept")
	public ResponseObject<List<FriendshipStatus>> accept(@RequestBody FriendshipStatus friendshipStatus,
			HttpServletRequest request) {
		String rsEmail = request.getAttribute("rsEmail").toString();
		Friendship friendship = friendshipService.findFriendship(rsEmail, friendshipStatus.getReceiveEmail());
		friendship.setStatus(true);
		friendshipService.save(friendship);
		return new ResponseObject<>(null, true, "Create list friend successfully.");
	}

	@PutMapping(value = "/decline")
	public ResponseObject<List<FriendshipStatus>> decline(@RequestBody FriendshipStatus friendshipStatus,
			HttpServletRequest request) {
		String rsEmail = request.getAttribute("rsEmail").toString();
		Friendship friendship = friendshipService.findFriendship(rsEmail, friendshipStatus.getReceiveEmail());
		friendshipService.delete(friendship);
		return new ResponseObject<>(null, true, "Delete friendship successfully.");
	}

	@GetMapping(value = "/getListRequest")
	public ResponseObject<List<FriendshipStatus>> getListRequest(HttpServletRequest request) {
		String rsEmail = request.getAttribute("rsEmail").toString();
		List<FriendshipStatus> listFriend = friendshipService.findAll(rsEmail, "");
		Iterator<FriendshipStatus> i = listFriend.iterator();
		while (i.hasNext()) {
			FriendshipStatus friend = i.next();
			if (friend.getSendEmail().equals(rsEmail) || friend.getStatus().equals("Friend"))
				i.remove();
		}
		return new ResponseObject<>(listFriend, true, "Get list friend successfully.");
	}
}
