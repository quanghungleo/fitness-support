package com.healthy.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
@Table(name = "friendship")
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Friendship implements Serializable {
  private static final long serialVersionUID = -7544923034512042159L;
  @Id
  @Column(name = "friendship_id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long friendshipId;

  @Column(name = "send_mail")
  private String sendMail;

  @Column(name = "receive_mail")
  private String receiveMail;

  @Column(name = "status")
  private Boolean status;
  
}
