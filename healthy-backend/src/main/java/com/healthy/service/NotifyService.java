package com.healthy.service;

import java.util.List;
import com.healthy.entity.Notify;
import com.healthy.model.NotifyDto;

public interface NotifyService {
  Notify saveNotify(Notify notify);
  List<Notify> getAllByEmail(String email);
  List<String> sendNotify(NotifyDto notifyDto);
}
