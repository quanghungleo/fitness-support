package com.healthy.service;

import java.util.List;

import com.healthy.entity.Friendship;
import com.healthy.model.FriendshipStatus;

public interface FriendshipService {
	Friendship save(Friendship friendship); 
	List<FriendshipStatus> findAll(String email,String search);
	List<FriendshipStatus> findAllFriend(String email,String search, Long date);
	List<FriendshipStatus> findAllFriendByStatus(String email,String search, Long date,Integer sttRemaining);
	Friendship findFriendship(String sendEmail, String receiveEmail);
	void delete(Friendship friendship);
}
