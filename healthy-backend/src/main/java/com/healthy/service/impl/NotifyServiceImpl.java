package com.healthy.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.healthy.entity.Notify;
import com.healthy.model.MessageDto;
import com.healthy.model.NotifyDto;
import com.healthy.model.NotifySendDto;
import com.healthy.repository.NotifyRepository;
import com.healthy.service.NotifyService;
import com.healthy.service.SegmentService;

@Service
public class NotifyServiceImpl implements NotifyService{
  @Autowired
  private NotifyRepository notifyRepository;
  
  @Autowired
  private SegmentService segmentService;

  @Override
  public Notify saveNotify(Notify notify) {
    return notifyRepository.save(notify);
  }

  @Override
  public List<Notify> getAllByEmail(String email) {
    return notifyRepository.findByEmail(email);
  }
  
  @Override
  public List<String> sendNotify(NotifyDto notifyDto) {
	  notifyDto.setCreatedDate(new Date().getTime());
	  final String uri = "https://onesignal.com/api/v1/notifications";
      final String appId = "c0d45dc5-22e2-438e-ad98-cca6044cd095";
      final String token = "Basic MDg2YjkwM2MtZmQ2MS00OGM3LThkM2YtODYxODgzYmQ0ZTI3";
      List<String> playerIdList = segmentService.getListPlayerIdByEmail(notifyDto.getEmail());
      MessageDto headings = MessageDto.builder().message(notifyDto.getTitle()).build();
      MessageDto content = MessageDto.builder().message(notifyDto.getMessage()).build();
      NotifySendDto notifySendDto = NotifySendDto.builder().appId(appId).contents(content).headings(headings).includePlayerIds(playerIdList).build();
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
      headers.setContentType(MediaType.APPLICATION_JSON);
      headers.set("author", token);
      HttpEntity<NotifySendDto> requestEntity = 
          new HttpEntity<>(notifySendDto, headers);
      RestTemplate restTemplate = new RestTemplate();
      try {
      restTemplate.exchange(uri, HttpMethod.POST, requestEntity, NotifySendDto.class);
      }catch(RestClientException e) {
      	e.printStackTrace();
      	return null;
      }
      return playerIdList;
  }
}
