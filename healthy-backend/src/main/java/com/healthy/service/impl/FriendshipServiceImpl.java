package com.healthy.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.healthy.entity.Friendship;
import com.healthy.model.FriendshipStatus;
import com.healthy.repository.FriendshipRepository;
import com.healthy.service.FriendshipService;

@Service
public class FriendshipServiceImpl implements FriendshipService {
	@Autowired
	private FriendshipRepository friendshipRepository;

	@Override
	public Friendship save(Friendship friendship) {
		return friendshipRepository.save(friendship);
	}

	@Override
	public List<FriendshipStatus> findAll(String email, String search) {
		List<FriendshipStatus> listFriend = convertToFriendshipStatusListModel(friendshipRepository.findFriendshipStatus(email, search));
		for(FriendshipStatus friend: listFriend) {
			List<String> currentFriends = friendshipRepository.findListEmailFriend(email);
			Integer size = currentFriends.size();
			List<String> listFriendReceive = friendshipRepository.findListEmailFriend(friend.getEmail());
			currentFriends.removeAll(listFriendReceive);
			friend.setMutualFriends(size-currentFriends.size());
		}
		return listFriend;
	}

	@Override
	public Friendship findFriendship(String sendEmail, String receiveEmail) {
		return friendshipRepository.findByBothEmail(sendEmail, receiveEmail);
	}

	@Override
	public List<FriendshipStatus> findAllFriend(String email, String search, Long date) {
		return convertToFriendshipStatusListModel(friendshipRepository.searchAllFriend(email, date, search));
	}

	@Override
	public List<FriendshipStatus> findAllFriendByStatus(String email, String search, Long date, Integer sttRemaining) {
		return convertToFriendshipStatusListModel(
				friendshipRepository.searchFriendBySttRemaining(email, sttRemaining, search, date));
	}

	@Override
	public void delete(Friendship friendship) {
		friendshipRepository.delete(friendship);
	}

	private FriendshipStatus convertToFriendshipStatusModel(Map<String, String> map) {
		FriendshipStatus friendship = new FriendshipStatus();
		friendship.setAvatarUrl(map.get("avatar_url"));
		friendship.setEmail(map.get("email"));
		friendship.setFullName(map.get("full_name"));
		friendship.setReceiveEmail(map.get("receive_mail"));
		friendship.setSendEmail(map.get("send_mail"));
		friendship.setWeightCurrent(map.get("weight"));
		friendship.setWeightStart(map.get("weight_start"));
		friendship.setWeightFinish(map.get("weight_finish"));
		friendship.setSttRemaining(map.get("stt_remaining"));
		if(null != map.get("status")) {
			if(map.get("status").equals("true")) {
				friendship.setStatus("Friend");
			}else if (map.get("status").equals("false")) {
				if(map.get("send_mail").equals(map.get("email"))) {
					friendship.setStatus("Accept available");
				}else {
					friendship.setStatus("Wait accept");
				}
			}
		}else {
			friendship.setStatus("Not friend");
		}
		return friendship;
	}

	private List<FriendshipStatus> convertToFriendshipStatusListModel(List<Map<String, String>> listMap) {
		List<FriendshipStatus> listFriendship = new ArrayList<>();
		for (Map<String, String> map : listMap) {
			listFriendship.add(convertToFriendshipStatusModel(map));
		}
		return listFriendship;
	}
}
