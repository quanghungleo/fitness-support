package com.healthy.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FriendshipStatus implements DTOEntity, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6448038407949889955L;
	@JsonProperty(value = "email")
	private String email;
	@JsonProperty(value = "status")
	private String status;
	@JsonProperty(value = "full_name")
	private String fullName;
	@JsonProperty(value = "avatar_url")
	private String avatarUrl;
	@JsonProperty(value = "weight_start")
	private String weightStart;
	@JsonProperty(value = "weight_current")
	private String weightCurrent;
	@JsonProperty(value = "weight_finish")
	private String weightFinish;
	@JsonProperty(value = "stt_remaining")
	private String sttRemaining;
	@JsonProperty(value = "send_email")
	private String sendEmail;
	@JsonProperty(value = "receive_email")
	private String receiveEmail;
	@JsonProperty(value = "mutual_friends")
	private Integer mutualFriends;
	
	
	public FriendshipStatus(String email, String status, String fullName, String avatarUrl) {
		super();
		this.email = email;
		this.status = status;
		this.fullName = fullName;
		this.avatarUrl = avatarUrl;
	}
	public FriendshipStatus(String email, String fullName, String avatarUrl, String weightStart, String weightCurrent) {
		super();
		this.email = email;
		this.fullName = fullName;
		this.avatarUrl = avatarUrl;
		this.weightStart = weightStart;
		this.weightCurrent = weightCurrent;
	}
	
	
	
}
