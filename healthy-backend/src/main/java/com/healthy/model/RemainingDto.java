package com.healthy.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RemainingDto {
    @JsonProperty(value = "date")
    private Long date;

    @JsonProperty(value = "status")
    private Integer status;
}
