import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./auth/auth.guard";

import { Role } from "./shared/account.model";

import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { FoodsComponent } from "./pages/foods/foods.component";
import { FoodEditComponent } from "./pages/food-edit/food-edit.component";
import { ExerciseComponent } from "./pages/exercise/exercise.component";
import { ExerciseEditComponent } from "./pages/exercise-edit/exercise-edit.component";
import { UserProfileComponent } from "./pages/user-profile/user-profile.component";
import { LoginComponent } from "./pages/login/login.component";
import { MainNavComponent } from "./main-nav/main-nav.component";
import { MainLayoutComponent } from "./layouts/main-layout/main-layout.component";
import { LoginLayoutComponent } from "./layouts/login-layout/login-layout.component";
import { GoalComponent } from "./pages/goal/goal.component";
import { StatisticsComponent } from "./pages/statistics/statistics.component";

const appRoutes: Routes = [
  {
    path: "",
    component: MainNavComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] },
    children: [
      {
        path: "",
        component: DashboardComponent
      },
      {
        path: "dashboard",
        component: DashboardComponent
      },
      { path: "foods", component: FoodsComponent },
      { path: "food-edit/:id", component: FoodEditComponent },
      { path: "exercise", component: ExerciseComponent },
      { path: "exercise-edit/:id", component: ExerciseEditComponent },
      { path: "user-profile", component: UserProfileComponent },
      { path: "all-goals", component: GoalComponent },
      { path: "user-by-remaining", component: StatisticsComponent }
    ]
  },
  {
    path: "",
    component: LoginLayoutComponent,
    children: [{ path: "login", component: LoginComponent }]
  },
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "**", redirectTo: "/login" }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(appRoutes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule {}
