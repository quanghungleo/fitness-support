import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";

import { LoginResponse } from "../shared/account.model";
import { environment } from "src/environments/environment.prod";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  endpoint: string = `${environment.URL_API}`;
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  private currentAccount: BehaviorSubject<LoginResponse>;
  public currentUser: Observable<LoginResponse>;

  constructor(private http: HttpClient, public router: Router) {
    this.currentAccount = new BehaviorSubject<LoginResponse>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentAccount.asObservable();
  }

  login(email: string, password: string) {
    return this.http
      .post<any>(this.endpoint + "/account/login", { email, password })
      .pipe(
        map(acc => {
          if (acc && acc.token) {
            localStorage.setItem("currentUser", JSON.stringify(acc));
            this.currentAccount.next(acc);
            return acc;
          }
        })
      );
  }

  public get currentAccountValue(): LoginResponse {
    return this.currentAccount.value;
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem("currentUser");
    return authToken !== null ? true : false;
  }

  logout() {
    let removeToken = localStorage.removeItem("currentUser");
    this.currentAccount.next(null);
    if (removeToken == null) {
      this.router.navigate(["login"]);
    }
  }

  handleError(error: HttpErrorResponse) {
    let msg = "";
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = "An error occurred:" + error.error.message;
      console.error(msg);
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.error}`;
      console.error(msg);
    }
    return throwError("Something bad happened; please try again later.");
  }
}
