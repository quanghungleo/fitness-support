export interface Notify {
  email: string;
  message: string;
  title: string;
}
