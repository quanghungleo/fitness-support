export interface UserByRemaining {
  account_id: number;
  user_name: string;
  email: string;
  status: number;
  created_at: Date;
  updated_at: Date;
}
