export interface Account {
  account_id: number;
  email: string;
  password: string;
  user_name: string;
  role: Role;
  status: string;
}

export enum Role {
  User = "user",
  Admin = "admin"
}

export interface LoginResponse {
  token?: string;
  expire: string;
  information: string;
  role: Role;
}
