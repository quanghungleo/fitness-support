import { Account } from "./account.model";

export interface Goal {
  activityLevel: number;
  calories: number;
  carbon: number;
  fat: number;
  month: number;
  proteins: number;
  type: number;
  weight: number;
  weightFinish: number;
  weightStart: number;
  account: Account;
}
