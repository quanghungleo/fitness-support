import { Account } from "./account.model";

export interface Exercise {
  exercise_id: number;
  calories_burn: number;
  exercise_name: string;
  index_of_sets: number;
  owner: string;
  repetitions: number;
  status: string;
  video_url: string;
  weight_per_set: number;
  account: Account;
}
