import { Account } from "./account.model";

export interface Food {
  food_id: number;
  description: string;
  food_name: string;
  quantity: number;
  unit: string;
  calories: number;
  carbs: number;
  proteins: number;
  fat: number;
  owner: string;
  account: Account;
}
