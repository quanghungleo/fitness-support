import { Account } from "./account.model";

export interface User {
  user_profile_id: number;
  full_name: string;
  email: string;
  avatar_url: string;
  gender: string;
  date_of_birth: Date;
  phone: string;
  weight: number;
  height: number;
  address: string;
  account: Account;
}
