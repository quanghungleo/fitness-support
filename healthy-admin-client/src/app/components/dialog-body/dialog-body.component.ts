import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { FormGroup, FormBuilder } from "@angular/forms";
import { first } from "rxjs/operators";
import { GoalService } from "src/app/services/goal.service";
import { NotificationService } from "../notification.service";
import { Notify } from "src/app/shared/notify.model";

@Component({
  selector: "app-dialog-body",
  templateUrl: "./dialog-body.component.html",
  styleUrls: ["./dialog-body.component.css"]
})
export class DialogBodyComponent implements OnInit {
  formNotify: FormGroup;
  message: string = "";
  title: string = "";

  constructor(
    private goalService: GoalService,
    private notifyService: NotificationService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogBodyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.formNotify = this.fb.group({
      message: [this.message, []],
      title: [this.title, []]
    });
  }

  save() {
    this.sendNotify();
    this.notifyService.success(":: Submitted successfully");
    this.close();
  }

  close() {
    this.formNotify.reset();
    this.dialogRef.close();
  }

  sendNotify(): void {
    var notify: Notify = this.data;
    console.log(this.data);

    this.goalService
      .sendNotify(notify)
      .pipe(first())
      .subscribe(res => {
        console.log(JSON.stringify(res));

        // this.isLoadingResults = true;
        this.ngOnInit();
      });
  }
}

export class ConfirmDialogModel {
  constructor(public title: string, public message: string) {}
}
