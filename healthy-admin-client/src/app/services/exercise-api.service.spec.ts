import { TestBed } from '@angular/core/testing';

import { ExerciseApiService } from './exercise-api.service';

describe('ExerciseApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExerciseApiService = TestBed.get(ExerciseApiService);
    expect(service).toBeTruthy();
  });
});
