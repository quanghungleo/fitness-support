import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Account } from "../shared/account.model";
import { map } from "rxjs/operators";
import { AuthService } from "../auth/auth.service";
import { environment } from "src/environments/environment.prod";

const apiUrl: string = `${environment.URL_API}`;

@Injectable({
  providedIn: "root"
})
export class AccountService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  getByToken() {
    let currentUser = this.authService.currentAccountValue;
    const newHeaders = new HttpHeaders({
      Authorization: `Bearer ${currentUser.token}`
    });
    return this.http
      .get<Account>(apiUrl + "/account/my-account", {
        headers: newHeaders
      })
      .pipe(
        map(acc => {
          return acc;
        })
      );
  }
}
