import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { catchError, tap, map, retry } from "rxjs/operators";

import { Exercise } from "../shared/exercise.model";
import { environment } from "src/environments/environment.prod";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

const apiUrl = `${environment.URL_API}/api/exercises`;

@Injectable({
  providedIn: "root"
})
export class ExerciseApiService {
  constructor(private http: HttpClient) {}

  getExercises(): Observable<any> {
    const url = `${apiUrl}/all`;
    return this.http.get<Exercise[]>(url);
  }

  addExercise(exercise): Observable<Exercise> {
    const url = `${apiUrl}/create`;
    return this.http.post<Exercise>(url, exercise, httpOptions);
  }

  getExercise(exerciseId: number): Observable<Exercise> {
    const url = `${apiUrl}/${exerciseId}`;
    return this.http.get<Exercise>(url);
  }

  updateExercise(exercise): Observable<any> {
    const url = `${apiUrl}/update`;
    return this.http.put(url, exercise, httpOptions);
  }

  deleteExercise(exerciseId): Observable<Exercise> {
    const url = `${apiUrl}/${exerciseId}`;

    return this.http.delete<Exercise>(url, httpOptions);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
