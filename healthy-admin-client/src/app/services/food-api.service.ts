import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, tap } from "rxjs/operators";
import { Food } from "../shared/food.model";
import { AuthService } from "../auth/auth.service";
import { environment } from "src/environments/environment.prod";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

const apiUrl = `${environment.URL_API}/api/foods`;

@Injectable({
  providedIn: "root"
})
export class FoodApiService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  getFoods(): Observable<any> {
    const url = `${apiUrl}/all-foods`;

    return this.http.get<Food[]>(url);
  }

  getFood(foodId: number): Observable<Food> {
    const url = `${apiUrl}/${foodId}`;

    return this.http.get<Food>(url);
  }

  addFood(food): Observable<Food> {
    const url = `${apiUrl}/create`;

    return this.http.post<Food>(url, food, httpOptions);
  }

  updateFood(food): Observable<any> {
    const url = `${apiUrl}/update`;
    console.log(food);

    return this.http.put(url, food, httpOptions);
  }

  deleteFood(id): Observable<Food> {
    const url = `${apiUrl}/${id}`;

    return this.http.delete<Food>(url, httpOptions);
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
