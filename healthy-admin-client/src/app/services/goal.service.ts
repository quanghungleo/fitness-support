import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, tap } from "rxjs/operators";
import { Goal } from "../shared/goal.model";
import { AuthService } from "../auth/auth.service";
import { environment } from "src/environments/environment.prod";
import { Notify } from "../shared/notify.model";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

const apiUrl = `${environment.URL_API}/api`;

@Injectable({
  providedIn: "root"
})
export class GoalService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  getAllGoals(): Observable<any> {
    // let currentUser = this.authService.currentAccountValue;
    // const newHeaders = new HttpHeaders({
    //   Authorization: `Bearer ${currentUser.token}`
    // });
    return this.http.get<Goal[]>(`${apiUrl}/all-goals`);
  }

  sendNotify(notify): Observable<Notify> {
    let currentUser = this.authService.currentAccountValue;
    httpOptions.headers = httpOptions.headers.set(
      "Authorization",
      `Bearer ${currentUser.token}`
    );
    return this.http.post<Notify>(`${apiUrl}/notify/`, notify, httpOptions);
  }

  //Tìm kiếm user by remaining
  getUserByRemaining(data: any): Observable<any> {
    console.log(data);

    return this.http.post<any>(
      `${apiUrl}/users-by-remaining`,
      data,
      httpOptions
    );
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
