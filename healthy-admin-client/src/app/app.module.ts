import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { jwtInterceptorProvider } from "./auth/authconfig.interceptor";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LayoutModule } from "@angular/cdk/layout";
import { FlexLayoutModule } from "@angular/flex-layout";

import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatCardModule,
  MatGridListModule,
  MatTableModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatSortModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatSnackBarModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatChipsModule
} from "@angular/material";

import { ChartsModule } from "ng2-charts";

import { MainNavComponent } from "./main-nav/main-nav.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { FoodsComponent } from "./pages/foods/foods.component";
import { FoodEditComponent } from "./pages/food-edit/food-edit.component";
import { ExerciseComponent } from "./pages/exercise/exercise.component";
import { ExerciseEditComponent } from "./pages/exercise-edit/exercise-edit.component";
import { UserProfileComponent } from "./pages/user-profile/user-profile.component";
import { LoginComponent } from "./pages/login/login.component";
import { MainLayoutComponent } from "./layouts/main-layout/main-layout.component";
import { LoginLayoutComponent } from "./layouts/login-layout/login-layout.component";
import { GoalComponent } from "./pages/goal/goal.component";
import { ErrorInterceptorProvider } from "./auth/error.interceptor";
import { DialogBodyComponent } from "./components/dialog-body/dialog-body.component";
import { StatisticsComponent } from "./pages/statistics/statistics.component";

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    DashboardComponent,
    FoodsComponent,
    FoodEditComponent,
    ExerciseComponent,
    ExerciseEditComponent,
    UserProfileComponent,
    LoginComponent,
    MainLayoutComponent,
    LoginLayoutComponent,
    GoalComponent,
    DialogBodyComponent,
    StatisticsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    FlexLayoutModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatChipsModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatGridListModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    AppRoutingModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  entryComponents: [DialogBodyComponent],
  providers: [jwtInterceptorProvider, ErrorInterceptorProvider],

  bootstrap: [AppComponent]
})
export class AppModule {}
