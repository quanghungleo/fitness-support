import { Component, OnInit } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";
import { Router } from "@angular/router";

import { AuthService } from "./../auth/auth.service";
import { AccountService } from "./../services/account.service";
import { Account, Role, LoginResponse } from "./../shared/account.model";

export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: "/dashboard", title: "Tổng quan", icon: "dashboard", class: "" },
  { path: "/foods", title: "Thực phẩm", icon: "fastfood", class: "" },
  { path: "/exercise", title: "Bài tập", icon: "fitness_center", class: "" },
  // {
  //   path: "/user-profile",
  //   title: "Quản lý tài khoản",
  //   icon: "people",
  //   class: ""
  // },
  {
    path: "/all-goals",
    title: "Mục tiêu người tập",
    icon: "timeline",
    class: ""
  },
  {
    path: "/user-by-remaining",
    title: "Theo dõi",
    icon: "timelapse",
    class: ""
  }
];

@Component({
  selector: "app-main-nav",
  templateUrl: "./main-nav.component.html",
  styleUrls: ["./main-nav.component.css"]
})
export class MainNavComponent implements OnInit {
  public menuItems: any[];

  // currentAccount: Account;
  loginResponse: LoginResponse;
  email_account = "";

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.accService.getByToken().subscribe(acc => {
      this.email_account = acc.email;
    });
  }

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private authService: AuthService,
    private router: Router,
    private accService: AccountService
  ) {}

  get isAdmin() {
    return (
      this.authService.currentAccountValue &&
      this.authService.currentAccountValue.role === Role.Admin
    );
  }

  onLogout() {
    this.authService.logout();
  }
}
