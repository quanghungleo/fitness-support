import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FoodApiService } from "../../services/food-api.service";
import {
  FormControl,
  FormGroupDirective,
  FormBuilder,
  FormGroup,
  NgForm,
  Validators
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { first } from "rxjs/operators";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: "app-food-edit",
  templateUrl: "./food-edit.component.html",
  styleUrls: ["./food-edit.component.css"]
})
export class FoodEditComponent implements OnInit {
  foodForm: FormGroup;
  id: number;
  // avt: TexImageSource = '';
  food_name: string = "";
  description: string = "";
  quantity: number;
  unit: string = "";
  calories: number;
  carbs: number;
  proteins: number;
  fat: number;
  isLoadingResults = false;

  matcher = new MyErrorStateMatcher();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: FoodApiService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.getFood(this.route.snapshot.params["id"]);
    this.foodForm = this.formBuilder.group({
      food_id: ["", Validators.required],
      food_name: ["", Validators.required],
      description: ["", Validators.required],
      quantity: ["", Validators.required],
      unit: ["", Validators.required],
      calories: ["", Validators.required],
      carbs: ["", Validators.required],
      proteins: ["", Validators.required],
      fat: ["", Validators.required]
    });
  }

  getFood(id) {
    this.api
      .getFood(id)
      .pipe(first())
      .subscribe(data => {
        console.log(data);

        this.id = data["data"].food_id;
        this.foodForm.setValue({
          food_id: data["data"].food_id,
          food_name: data["data"].food_name,
          description: data["data"].description,
          quantity: data["data"].quantity,
          unit: data["data"].unit,
          calories: data["data"].calories,
          carbs: data["data"].carbs,
          proteins: data["data"].proteins,
          fat: data["data"].fat
        });
      });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api
      .updateFood(form)
      .pipe(first())
      .subscribe(
        res => {
          this.id = res["food_id"];
          this.isLoadingResults = false;
          this.router.navigate(["/foods"]);
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }
}
