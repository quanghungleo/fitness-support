import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { UserByRemaining } from "src/app/shared/user-by-remaining";
import { GoalService } from "src/app/services/goal.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  MatSnackBar,
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialogConfig,
  MatDialog
} from "@angular/material";
import { first, map } from "rxjs/operators";
import { MyErrorStateMatcher } from "../foods/foods.component";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";
import { DialogBodyComponent } from "src/app/components/dialog-body/dialog-body.component";

@Component({
  selector: "app-statistics",
  templateUrl: "./statistics.component.html",
  styleUrls: ["./statistics.component.css"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class StatisticsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ["email", "control"];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  isHaveTarget = false;

  dataSource: any;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  date: Date;

  countOne = 0;
  countTwo = 0;
  countThree = 0;
  countFour = 0;

  constructor(
    private fb: FormBuilder,
    private goalService: GoalService,
    private matDialog: MatDialog
  ) {}

  matcher = new MyErrorStateMatcher();

  filterUserForm: FormGroup;
  ngOnInit() {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    this.date = today;
    this.getTotalRemaining(this.date.getTime());
    this.filterUserForm = this.fb.group({
      date: [null, Validators.required],
      status: [null, Validators.required]
    });
  }

  ngAfterViewInit() {
    // const today = new Date();
    // today.setHours(0, 0, 0, 0);
    // this.date = today;
    // this.getTotalRemaining(this.date.getTime());
  }

  getTotalRemaining(date) {
    this.isLoadingResults = true;
    this.isHaveTarget = false;
    this.goalService
      .getUserByRemaining({ date: date, status: 1 })
      .pipe(first())
      .subscribe(res => {
        this.dataSource = res.lists;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isRateLimitReached = false;
        this.isLoadingResults = false;

        if (res.lists.length != 0) {
          this.isHaveTarget = true;
        }

        this.countOne = res.lists.length;
        this.resultsLength = res.lists.length;
        console.log(this.countOne);
      });

    this.goalService
      .getUserByRemaining({ date: date, status: 2 })
      .pipe(first())
      .subscribe(res => {
        this.dataSource = res.lists;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isRateLimitReached = false;
        this.isLoadingResults = false;

        if (res.lists.length != 0) {
          this.isHaveTarget = true;
        }

        this.countTwo = res.lists.length;
        this.resultsLength = res.lists.length;
        console.log(this.countTwo);
      });
    this.goalService
      .getUserByRemaining({ date: date, status: 3 })
      .pipe(first())
      .subscribe(res => {
        this.dataSource = res.lists;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isRateLimitReached = false;
        this.isLoadingResults = false;

        if (res.lists.length != 0) {
          this.isHaveTarget = true;
        }

        this.countThree = res.lists.length;
        this.resultsLength = res.lists.length;
        console.log(this.countThree);
      });

    this.goalService
      .getUserByRemaining({ date: date, status: 4 })
      .pipe(first())
      .subscribe(res => {
        this.dataSource = new MatTableDataSource(res.lists);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isRateLimitReached = false;
        this.isLoadingResults = false;

        if (res.lists.length != 0) {
          this.isHaveTarget = true;
        }

        this.countFour = res.lists.length;
        this.resultsLength = res.lists.length;
        console.log(res.lists);
      });
  }

  filterUserByRemaining() {
    this.isLoadingResults = true;
    this.isHaveTarget = false;
    const dateTimestamp: Date = this.filterUserForm.value.date;
    dateTimestamp.setHours(0, 0, 0, 0);

    const value = {
      date: dateTimestamp.getTime(),
      status: this.filterUserForm.value.status
    };
    this.goalService
      .getUserByRemaining(value)
      .pipe(first())
      .subscribe(res => {
        console.log(res);

        this.dataSource = new MatTableDataSource(res.lists);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isRateLimitReached = false;
        this.isLoadingResults = false;

        if (res.lists.length != 0) {
          this.isHaveTarget = true;
        }

        this.resultsLength = res.lists.length;
        if (value.status == 4) {
          this.countFour = res.lists.length;
        } else if (value.status == 3) {
          this.countThree = res.lists.length;
        } else if (value.status == 2) {
          this.countTwo = res.lists.length;
        } else if (value.status == 1) {
          this.countOne = res.lists.length;
        }
      });
  }

  openDialog(email) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = { email: email };

    dialogConfig.width = "30%";

    this.matDialog.open(DialogBodyComponent, dialogConfig);
  }

  searchKey: string;
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  // search on table
  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
