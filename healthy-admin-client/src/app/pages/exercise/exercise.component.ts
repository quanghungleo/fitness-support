import { Component, OnInit, AfterViewInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { ErrorStateMatcher } from "@angular/material/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import {
  FormControl,
  FormGroupDirective,
  FormBuilder,
  FormGroup,
  NgForm,
  Validators
} from "@angular/forms";

import { Exercise } from "../../shared/exercise.model";
import { ExerciseApiService } from "../../services/exercise-api.service";
import { first } from "rxjs/operators";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
  selector: "app-exercise",
  templateUrl: "./exercise.component.html",
  styleUrls: ["./exercise.component.css"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class ExerciseComponent implements OnInit, AfterViewInit {
  constructor(
    private fb: FormBuilder,
    private exApiService: ExerciseApiService,
    private _snackBar: MatSnackBar
  ) {}

  displayedColumns: string[] = [
    "exercise_id",
    "exercise_name",
    "calories_burn",
    "index_of_sets",
    "repetitions",
    "weight_per_set"
  ];
  // dataExerciseApi: ExerciseApiService | null;
  // dataExercise: Exercise[];
  dataSource: MatTableDataSource<Exercise>;

  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  matcher = new MyErrorStateMatcher();

  exerciseForm: FormGroup;
  ngOnInit() {
    this.exerciseForm = this.fb.group({
      exercise_name: [null, Validators.required],
      calories_burn: [null, Validators.required],
      index_of_sets: [null, Validators.required],
      repetitions: [null, Validators.required],
      weight_per_set: [null, Validators.required],
      video_url: [null, Validators.required]
    });
  }

  onFormSubmit() {
    this.isLoadingResults = true;
    this.exApiService
      .addExercise(this.exerciseForm.value)
      .pipe(first())
      .subscribe(
        res => {
          console.log(JSON.stringify(res));

          let id = res["exercise_id"];
          // this.isLoadingResults = true;
          this.ngAfterViewInit();
          // this.router.navigate(["/exercise"]);
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  getExercises(): void {
    this.exApiService
      .getExercises()
      .pipe(first())
      .subscribe(
        resultArray => {
          this.dataSource = new MatTableDataSource(resultArray.data);
          console.log(this.dataSource);

          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.isLoadingResults = false;
        },
        error => console.error(`Error :: ${JSON.stringify(error)}`)
      );
  }

  deleteExercise(id: any) {
    this.isLoadingResults = true;
    this.exApiService
      .deleteExercise(id)
      .pipe(first())
      .subscribe(
        result => {
          // this.isLoadingResults = false;
          this._snackBar.open(`Đã xoá bài tập ${result.exercise_name}`);
          this.ngAfterViewInit();
        },
        err => {
          console.error(err);
          this.isLoadingResults = false;
        }
      );
  }

  ngAfterViewInit() {
    this.getExercises();
  }

  searchKey: string;
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
