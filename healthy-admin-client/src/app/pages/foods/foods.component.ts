import {
  Component,
  AfterViewInit,
  ViewChild,
  OnInit,
  ChangeDetectorRef
} from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatSnackBar } from "@angular/material/snack-bar";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";
import {
  FormControl,
  FormGroupDirective,
  FormBuilder,
  FormGroup,
  NgForm,
  Validators
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";

import { FoodApiService } from "../../services/food-api.service";
import { Food } from "../../shared/food.model";
import { first } from "rxjs/operators";

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: "app-foods",
  templateUrl: "./foods.component.html",
  styleUrls: ["./foods.component.css"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class FoodsComponent implements OnInit, AfterViewInit {
  constructor(
    private fb: FormBuilder,
    private foodService: FoodApiService,
    private _snackBar: MatSnackBar,
    private _httpClient: HttpClient,
    private cdr: ChangeDetectorRef
  ) {}

  search = "";
  totalWeight = 0;
  caloriesTarget = 0;
  panelOpenState = false;
  dataTarget = null;

  displayedColumns: string[] = [
    "food_name",
    "unit",
    "quantity",
    "carbs",
    "fat",
    "proteins",
    "calories"
  ];
  // dataFoodsApi: ApiService | null;
  // dataFoods: Food[];
  dataSearch: MatTableDataSource<Food>;

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  displayedColumnsTarget: string[] = ["label", "weight", "unit"];
  dataSourceTarget: DataNutri[] = [];
  isLoadingTarget = false;
  isHaveTarget = false;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  matcher = new MyErrorStateMatcher();

  foodForm: FormGroup;
  ngOnInit() {
    this.foodForm = this.fb.group({
      food_name: [null, Validators.required],
      description: [null, Validators.required],
      quantity: [null, Validators.required],
      unit: [null, Validators.required],
      calories: [null, Validators.required],
      carbs: [null, Validators.required],
      proteins: [null, Validators.required],
      fat: [null, Validators.required]
    });
  }

  onFormSubmit() {
    this.isLoadingResults = true;
    this.foodService
      .addFood(this.foodForm.value)
      .pipe(first())
      .subscribe(
        res => {
          let id = res["food_id"];
          this.isLoadingResults = true;
          this.ngAfterViewInit();
          // this.router.navigate(["/foods"]);
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  deleteFood(id: any) {
    this.isLoadingResults = true;
    this.foodService
      .deleteFood(id)
      .pipe(first())
      .subscribe(
        res => {
          this._snackBar.open(`Đã xoá ${res.food_name}!`);
          this.ngAfterViewInit();
          // this.router.navigate(["/foods"]);
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  loadFoods(): any {
    this.foodService
      .getFoods()
      .pipe(first())
      .subscribe(
        res => {
          // console.log(data);

          this.resultsLength = res.data.length;
          this.dataSearch = new MatTableDataSource(res.data);
          this.dataSearch.paginator = this.paginator;
          this.dataSearch.sort = this.sort;
          this.isLoadingResults = false;

          // this.dataSearch.filterPredicate = (data, filter) => {
          //   return this.displayedColumns.some(ele => {
          //     return (
          //       ele != "actions" && data[ele].toLowerCase().indexOf(filter) != -1
          //     );
          //   });
          // };
        },
        error => console.error(`Error : ${JSON.stringify(error)}`)
      );
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
    this.loadFoods();
  }

  searchKey: string;
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  // search on table
  applyFilter() {
    this.dataSearch.filter = this.searchKey.trim().toLowerCase();
    if (this.dataSearch.paginator) {
      this.dataSearch.paginator.firstPage();
    }
  }

  searchEnter(event) {
    this.dataSourceTarget = [];
    this.isLoadingTarget = true;
    this.isHaveTarget = false;
    const href = "https://edamam-edamam-nutrition-analysis.p.rapidapi.com";
    const requestUrl = `${href}/api/nutrition-data?ingr=${encodeURI(
      event.target.value
    )}`;
    this.getFoodTarget(requestUrl).subscribe(data => {
      for (var key in data["totalNutrients"]) {
        this.dataSourceTarget.push(data["totalNutrients"][key]);
      }
      console.log(this.dataSourceTarget);
      if (this.dataSourceTarget.length != 0) {
        this.caloriesTarget = data["calories"];
        this.totalWeight = data["totalWeight"];
        this.search = event.target.value;
        this.isHaveTarget = true;
      }
      this.isLoadingTarget = false;
    });
  }

  getFoodTarget(url) {
    const httpOptions = {
      headers: new HttpHeaders({
        "x-rapidapi-host": "edamam-edamam-nutrition-analysis.p.rapidapi.com",
        "x-rapidapi-key": "9e81014292mshb0aa35aa31980e5p132c2cjsneee35e559327"
      })
    };
    return this._httpClient.get(url, httpOptions);
  }
}

export interface DataNutri {
  label: string;
  quantity: number;
  unit: string;
}
