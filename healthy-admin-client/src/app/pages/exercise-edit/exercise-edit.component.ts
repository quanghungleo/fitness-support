import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ExerciseApiService } from "../../services/exercise-api.service";
import {
  FormControl,
  FormGroupDirective,
  FormBuilder,
  FormGroup,
  NgForm,
  Validators
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { first } from "rxjs/operators";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: "app-exercise-edit",
  templateUrl: "./exercise-edit.component.html",
  styleUrls: ["./exercise-edit.component.css"]
})
export class ExerciseEditComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ExerciseApiService,
    private formBuilder: FormBuilder
  ) {}

  exerciseForm: FormGroup;
  id: number;
  exercise_name: string = "";
  calories_burn: number;
  index_of_sets: number;
  repetitions: number;
  video_url: string = "";
  weight_per_set: number;
  isLoadingResults = false;

  matcher = new MyErrorStateMatcher();
  ngOnInit() {
    this.getExercise(this.route.snapshot.params["id"]);
    this.exerciseForm = this.formBuilder.group({
      exercise_id: ["", Validators.required],
      exercise_name: ["", Validators.required],
      calories_burn: ["", Validators.required],
      index_of_sets: ["", Validators.required],
      repetitions: ["", Validators.required],
      weight_per_set: ["", Validators.required],
      video_url: ["", Validators.required]
    });
  }

  getExercise(id: number) {
    this.api
      .getExercise(id)
      .pipe(first())
      .subscribe(data => {
        console.log(data);

        this.id = data["data"].exercise_id;
        this.exerciseForm.setValue({
          exercise_id: this.id,
          exercise_name: data["data"].exercise_name,
          calories_burn: data["data"].calories_burn,
          index_of_sets: data["data"].index_of_sets,
          repetitions: data["data"].repetitions,
          weight_per_set: data["data"].weight_per_set,
          video_url: data["data"].video_url
        });
      });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api
      .updateExercise(form)
      .pipe(first())
      .subscribe(
        res => {
          this.id = res["id"];
          this.isLoadingResults = false;
          this.router.navigate(["/exercise"]);
        },
        err => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }
}
