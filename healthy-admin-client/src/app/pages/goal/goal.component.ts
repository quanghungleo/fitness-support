import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatSnackBar } from "@angular/material/snack-bar";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";

import { GoalService } from "../../services/goal.service";
import { Goal } from "../../shared/goal.model";
import { first } from "rxjs/operators";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { DialogBodyComponent } from "src/app/components/dialog-body/dialog-body.component";
import { ChartDataSets } from "chart.js";
import { Label, Color } from "ng2-charts";

@Component({
  selector: "app-goal",
  templateUrl: "./goal.component.html",
  styleUrls: ["./goal.component.css"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class GoalComponent implements OnInit {
  Math = Math;
  constructor(
    private goalService: GoalService,
    private _snackBar: MatSnackBar,
    private matDialog: MatDialog
  ) {}

  displayedColumns: string[] = [
    "email",
    "updated_at",
    "activity_level",
    "month",
    "weight_start",
    "weight",
    "weight_finish",
    "control"
  ];

  dataGoal: MatTableDataSource<Goal>;

  isLoadingResults = true;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  ngOnInit() {
    this.getAllGoals();
  }

  openDialog(email) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = { email: email };

    dialogConfig.width = "30%";

    this.matDialog.open(DialogBodyComponent, dialogConfig);
  }

  getAllGoals(): void {
    this.goalService
      .getAllGoals()
      .pipe(first())
      .subscribe(
        resultArr => {
          this.dataGoal = new MatTableDataSource(resultArr.data);
          console.log(resultArr["data"]);

          console.log(this.dataGoal);

          this.dataGoal.paginator = this.paginator;
          this.dataGoal.sort = this.sort;
          this.isLoadingResults = false;
        },
        err => console.error(`Lỗi rồi => ${JSON.stringify(err)}`)
      );
  }

  searchKey: string;
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.dataGoal.filter = this.searchKey.trim().toLowerCase();

    if (this.dataGoal.paginator) {
      this.dataGoal.paginator.firstPage();
    }
  }
}
