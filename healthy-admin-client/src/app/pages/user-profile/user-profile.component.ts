import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatSnackBar } from "@angular/material/snack-bar";
import {
  animate,
  state,
  style,
  transition,
  trigger
} from "@angular/animations";

import { UserProfileService } from "../../services/user-profile.service";
import { User } from "../../shared/user.model";
@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      )
    ])
  ]
})
export class UserProfileComponent implements OnInit, AfterViewInit {
  constructor(
    private profileService: UserProfileService,
    private _snackBar: MatSnackBar
  ) {}

  displayedColumns: string[] = [
    "full_name",
    "email",
    "gender",
    "date_of_birth",
    "phone"
  ];

  dataProfile: MatTableDataSource<User>;

  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  ngOnInit() {}

  ngAfterViewInit() {
    this.getAllUserProfile();
  }

  getAllUserProfile(): void {
    this.profileService.getUserProfile().subscribe(
      resultArr => {
        this.dataProfile = new MatTableDataSource(resultArr.data);

        this.dataProfile.paginator = this.paginator;
        this.dataProfile.sort = this.sort;
        this.isLoadingResults = false;
      },
      error => console.error(`Error :: ${JSON.stringify(error)}`)
    );
  }

  searchKey: string;
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.dataProfile.filter = this.searchKey.trim().toLowerCase();

    if (this.dataProfile.paginator) {
      this.dataProfile.paginator.firstPage();
    }
  }
}
