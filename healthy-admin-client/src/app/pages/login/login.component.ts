import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { first } from 'rxjs/operators';

import { AuthService } from "../../auth/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.form = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required]
    });
  }

  form: FormGroup;
  private formSubmitAttempt: boolean;
  showSpinner = false;
	// submitted = false;
	returnUrl: string;
	error = '';

  ngOnInit() {
    this.authService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
	get f() { return this.form.controls; }

  isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

  onSubmit() {
    this.showSpinner = true;

    this.form.value.action = 'login';
    this.authService.login(this.f.email.value, this.f.password.value)
    .pipe(first())
    .subscribe( data => {
      this.router.navigate([this.returnUrl])
    }, err => {
      this.error = err.error.message;
      // console.log(JSON.stringify(err));
      this.showSpinner = false;
    })
  }
}
